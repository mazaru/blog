---
title: tbd
email: mazaru@protonmail.com
gpg: 24D42C59D995E77FAA597205EFC59519270B8407 
---

**rss**: <https://tbd/rss.xml>

**src**: <https://gitlab.com/mazaru/blog>

---

**email**: <mazaru@protonmail.com>

**gpg**: [24D4 2C59 D995 E77F AA59 7205 EFC5 9519 270B 8407](https://gitlab.com/mazaru/blog/-/snippets/2148044)

---

**Built with**: <https://gitlab.com/mazaru/site-gen>
